The `Spinner` displays an icon-size spinning animation. It is often used as an alternative to a `ProgressBar` for
displaying indefinite activity, instead of actual progress.

To start the animation, use `Spinner.start`, to stop it use `Spinner.stop`.

**Example**

``` kotlin title="samples/gtk/widgets/src/nativeMain/kotlin/org/gtkkn/samples/gtk/widgets/Spinner.kt"
--8<-- "samples/gtk/widgets/src/nativeMain/kotlin/org/gtkkn/samples/gtk/widgets/Spinner.kt"
```
